/**
 * Created by sberdibekov on 05.11.15.
 */
public class Pump {

    private double n;
    private double D_2;
    private double n_s;
    private double H_0;
    private double a;
    private double b;
    private double c_0;
    private double c_1;
    private double c_2;

    /**
     * Насос
     * @param n Оборот (об/ч)
     * @param d_2 Наружный диметр рабочего колеса (м)
     * @param n_s коэффицент быстроходности насоса
     * @param h_0 напор (м)
     * @param a эмирический коэффицент (ч/м2)
     * @param b эмирический коэффицент (ч^2/м^5)
     * @param c_0 эмирический коэффицент
     * @param c_1 эмирический коэффицент (ч/м^3)
     * @param c_2 эмирический коэффицент (ч^3/м^6)
     */
    public Pump(double n, double d_2, double n_s, double h_0, double a, double b, double c_0, double c_1, double c_2) {
        this.n = n;
        D_2 = d_2;
        this.n_s = n_s;
        H_0 = h_0;
        this.a = a;
        this.b = b;
        this.c_0 = c_0;
        this.c_1 = c_1;
        this.c_2 = c_2;
    }

    /**
     * Пересчитываем подачу для другой жидкости (нефть)
     * @param v кинематическая вязкость (м^2/с)
     * @return подача
     */
    public double recalculate(double v) {

        double Re = n * D_2 * D_2 / v;

        double Re_n = 3.16 * 100000 * Math.pow(n_s, -0.305);

        double Re_gr = 0.224 * 100000 * Math.pow(n_s, 0.384);

        double a_n = 1.33 * Math.pow(n_s, -0.326);

        double k_H = 1 - 0.128 * Math.log10(Re_n / Re);

        double k_Q = Math.pow(k_H, 1.5);

        double k_n = 1 - a_n * Math.log10(Re_gr / Re);

        double H_ov = k_H * H_0;

        double b_v = b * k_H / (k_Q * k_Q);

        double c_0v = k_n * c_0;

        double c_1v = c_1 * k_n / k_Q;

        double c_2v = c_2 * k_n / (k_Q * k_Q);

        double Q_v_opt = -c_1v / (2 * c_2v);

        return Q_v_opt;
    }

}
