/**
 * Created by sberdibekov on 05.11.15.
 */
public class Main {

    public static void main(String[] args) {

        Pump HM1250_260 = new Pump(50, 0.418, 59, 283, 0, 0.0000354, 0.1714, 0.001191, -0.0000005268);
        /*
        System.out.println(HM1250_260.recalculate(150. / 1000000)); //Пример

        System.out.println(HM1250_260.recalculate(53.1 / 1000000)); //Минимальная вязкость нефти Каражанбаса
        System.out.println(HM1250_260.recalculate(259.45 / 1000000)); //Минимальная вязкость нефти Каражанбаса
        System.out.println();

        */

        Pump _12HDcH_1 = new Pump(30, 0.46, 100, 33.4, 0, 0.00000858, 0.0614, 0.00205, -0.0000000126);
        Pump _12HDcH_2 = new Pump(30, 0.43, 105, 29.4, 0, 0.00000947, 0.1280, 0.00196, -0.0000000127);
        Pump _12HDcH_3 = new Pump(30, 0.40, 120, 24.1, 0, 0.00000984, 0.1190, 0.00220, -0.0000000158);

        System.out.println(_12HDcH_1.recalculate(53.1 / 1000000)); //Минимальная вязкость нефти Каражанбаса
        System.out.println(_12HDcH_1.recalculate(259.45 / 1000000)); //Минимальная вязкость нефти Каражанбаса
        System.out.println();

        /*
        System.out.println(_12HDcH_2.recalculate(53.1 / 1000000)); //Минимальная вязкость нефти Каражанбаса
        System.out.println(_12HDcH_2.recalculate(259.45 / 1000000)); //Минимальная вязкость нефти Каражанбаса
        System.out.println();

        System.out.println(_12HDcH_3.recalculate(53.1 / 1000000)); //Минимальная вязкость нефти Каражанбаса
        System.out.println(_12HDcH_3.recalculate(259.45 / 1000000)); //Минимальная вязкость нефти Каражанбаса
        System.out.println();
        */

    }

}
